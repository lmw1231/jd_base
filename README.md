## Docker hub 镜像拉取一键运行脚本

 docker run -dit \
 -v /home/jd/config:/jd/config \
 -v /home/jd/log:/jd/log \
 -v /home/jd/scripts:/jd/scripts \
 -p 5678:5678 \
 -e ENABLE_HANGUP=true \
 -e ENABLE_WEB_PANEL=true \
 --name jd \
 --hostname jd \
 --restart always \
 haileishui/lmw:latest
 
### docker push镜像到远程服务器
docker build -t haileishui/lmw:latest .
docker tag image haileishui/lmw:latest 
docker push haileishui/lmw:latest

### 推送到阿里云仓库

# 登录
$ sudo docker login --username=xxxx registry.cn-hangzhou.aliyuncs.com
# docker tag 标记本地镜像，将其归入某一仓库。
$ sudo docker tag [ImageId] registry.cn-hangzhou.aliyuncs.com/makalochen/tomcat:[镜像版本号]
# 推送
$ sudo docker push registry.cn-hangzhou.aliyuncs.com/makalochen/tomcat:[镜像版本号]


## 请仔细阅读 [WIKI](https://github.com/EvineDeng/jd-base/wiki) 和各文件注释，95%的问题都能找到答案
## lmw二次开发
## 如有二次使用，请注明来源

本脚本是以下两个仓库的shell套壳工具：

[LXK9301/jd_scripts](https://github.com/LXK9301/jd_scripts)：主要是长期任务。

[shylocks/Loon](https://github.com/shylocks/Loon)：主要是短期任务、一次性任务，正因为是短期的和一次性的，所以经常会有报错，报错就报错了，不要催我也不要去催[shylocks](https://github.com/shylocks)大佬。

## 适用于以下系统

- ArmBian/Debian/Ubuntu/OpenMediaVault/CentOS/Fedora/RHEL等Linux系统

- OpenWRT

- Android

- MacOS

- Docker

## 说明

1. 宠汪汪赛跑助力先让用户提供的各个账号之间相互助力，助力完成你提供的所有账号以后，再给我和lxk0301大佬助力，每个账号助力后可得30g狗粮。

2. 将部分临时活动修改为了我的邀请码，已取得lxk0301大佬的同意。

## 如有帮助你薅到羊毛，请不吝赏作者一杯茶水费

![thanks](https://github.com/EvineDeng/jd-base/wiki/Picture/thanks.png)

## 更新日志

> 只记录大的更新，小修小改不记录。

2021-01-23，控制面板增加日志查看功能，Docker重启容器后可以使用`docker restart jd`，非Docker如果是pm2方式的请重启pm2进程`pm2 resatrt server.js`。

2020-01-21，增加shylocks/Loon脚本。

2021-01-15，如果本机上安装了pm2，则挂机程序以pm2启动，否则以nohup启动。
